
Взять логику из задачи домашнего задания №8(task5). 

Реализовать REST сервис TerminalController, c помощью которого можно:
1) Проверить состояние счета
2) Снять/ положить деньги

Доступ к терминалу должен предоставлять только после корректного ввода пина. При попытке вызова любого метода без ввода
пина, должен кидаться ексепшен. 

При вводе 3 неправильных пинов, аккаунт лочится на 5сек( при попытке обраться к нему вылетает AccountIsLockedException 
c информацией о времени разлочения).

Класть и снимать деньги можно только, если сумма кратна 100. 

Интерфейс терминала и список исключений остается на ваш дизайн. 

В каждом ексепшене должно быть описание, что нужно сделать, чтобы избежать его в дальнейшем.

Это все должно быть реализовано с использованием spring framework , Spring boot использовать ЗАПРЕЩЕНО! Сам терминал 
должен быть сервисом и инжектиться в TerminalController, который в свою очередь является REST сервисом. Не забывайте, 
не все бины потокобезопасны (вы теперь совсем взрослые, основной критерий приемки , это что бы код был потокобезопасен).
Хранилище клиентов сделать упрощенным, обычная in memory, concurrentMapa в синглтоне будет достатоно (можно придумать 
решение получше, но бд пока что не надо, успеете).

Каждый запрос и ответ к сервису должен логироваться (тут должен быть намек на использование аспекта). 
UI реализовывать не нужно! Достаточно просто бросать простейшие REST запросы к серверу, для генерации запроса 
использовать soapui или любой другой rest клиент.
