package ru.sberbank.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

public interface TerminalController {
    ResponseEntity<?> login(String idClient, String pinCode);

    ResponseEntity<?> presetSummAccount(@PathVariable("idClient") String idClient);

    ResponseEntity<?> creditOfAccount(@PathVariable("idClient") String idClient, int summ);

    ResponseEntity<?> depositOfAccount(@PathVariable("idClient") String idClient, int summ);
}
