package ru.sberbank.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sberbank.model.impl.TerminalService;

@RestController
public class TerminalControllerImpl implements TerminalController {

    private final TerminalService service;

    @Autowired
    public TerminalControllerImpl(TerminalService service) {
        this.service = service;
    }

    @PostMapping(value = "/login")
    public ResponseEntity<?> login(String idClient, String pinCode) {
        try {
            service.login(idClient, pinCode);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/clients/{idClient}")
    public ResponseEntity<?> presetSummAccount(@PathVariable("idClient") String idClient) {
        service.presetSummAccount(idClient);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/clients/{idClient}")
    public ResponseEntity<?> creditOfAccount(@PathVariable("idClient") String idClient, int summ) {
        try {
            service.creditOfAccount(idClient, summ);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/clients/{idClient}")
    public ResponseEntity<?> depositOfAccount(@PathVariable("idClient") String idClient, int summ) {
        try {
            service.depositOfAccount(idClient, summ);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
