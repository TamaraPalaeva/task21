package ru.sberbank.controller.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;

import java.util.logging.Logger;

@Aspect
public class LoggingAspect {

    private static Logger LOG = Logger.getLogger(LoggingAspect.class.getName());

    @Pointcut("execution(* *.TerminalControllerImpl.*(..))")
    private void allLogEventMethods() {
    }

    @AfterReturning(pointcut = "allLogEventMethods()",
            returning = "returnVal")
    public void logAfter(Object returnVal) {
        LOG.info("AFTER_RET: " + returnVal);
    }

    @AfterThrowing(pointcut = "allLogEventMethods()",
            throwing = "ex")
    public void logAfterThrow(Throwable ex) {
        LOG.warning("Throw: " + ex.getMessage());
    }

    @Around(value = "allLogEventMethods()")
    public void logMethod(ProceedingJoinPoint joinPoint) {
        LOG.info("Открытие операции...");
        try {
            joinPoint.proceed();
            LOG.info("Закрытие операции....");
        } catch (Throwable throwable) {
            LOG.warning("Операция не удалась. " + throwable.getMessage());
        }
    }
}
