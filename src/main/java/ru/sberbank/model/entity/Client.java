package ru.sberbank.model.entity;

import java.util.Date;
import java.util.Objects;

public class Client {
    private String name;
    private String id;
    private String pinCode;
    private int attempts = 3;
    private Date dateFinishedBlock;
    private boolean loginClient = false;
    private Integer summAccount;

    public boolean isLoginClient() {
        return loginClient;
    }

    public void setLoginClient(boolean loginClient) {
        this.loginClient = loginClient;
    }

    public Client(String name, String id, String pinCode, Integer summAccount) {
        this.name = name;
        this.id = id;
        this.pinCode = pinCode;
        this.summAccount = summAccount;
    }

    public Integer getSummAccount() {
        return summAccount;
    }

    public void setSummAccount(Integer summAccount) {
        this.summAccount = summAccount;
    }

    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public int getAttempts() {
        return attempts;
    }

    public void setAttempts(int attempts) {
        this.attempts = attempts;
    }

    public Date getDateFinishedBlock() {
        return dateFinishedBlock;
    }

    public void setDateFinishedBlock(Date dateFinishedBlock) {
        this.dateFinishedBlock = dateFinishedBlock;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return attempts == client.attempts &&
                loginClient == client.loginClient &&
                Objects.equals(name, client.name) &&
                Objects.equals(id, client.id) &&
                Objects.equals(pinCode, client.pinCode) &&
                Objects.equals(dateFinishedBlock, client.dateFinishedBlock) &&
                Objects.equals(summAccount, client.summAccount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, id, pinCode, attempts, dateFinishedBlock, loginClient, summAccount);
    }
}
