package ru.sberbank.model.entity;

import java.util.HashMap;
import java.util.Map;

public class ClientDAO {
    private static ClientDAO clientDAO;
    private Map<String, Client> clientMap;

    private ClientDAO() {
        this.clientMap = new HashMap<>();
        clientMap.put("1", new Client("Ivanod", "1", "9876", 6542));
        clientMap.put("2", new Client("Petrov", "2", "7412", 254010));
        clientMap.put("3", new Client("Sidorov", "3", "6523", 685300));
    }

    public static ClientDAO initializeClientDAO() {
        if (clientDAO == null) {
            clientDAO = new ClientDAO();
        }
        return clientDAO;
    }

    public Map<String, Client> getClientMap() {
        return clientMap;
    }

    public void addClientToClientDAO(Client client) {
        this.clientMap.put(client.getId(), client);
    }
}
