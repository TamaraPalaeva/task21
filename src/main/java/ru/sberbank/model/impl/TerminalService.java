package ru.sberbank.model.impl;

import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.Client;
import ru.sberbank.model.service.PinValidator;
import ru.sberbank.model.service.Terminal;
import ru.sberbank.view.exception.AccountIsLockedException;
import ru.sberbank.view.exception.TransitException;

@Service
public class TerminalService implements Terminal {
    private final TerminalServer server;
    private final PinValidator pinValidator;

    public TerminalService(TerminalServer server, PinValidator pinValidator) {
        this.server = server;
        this.pinValidator = pinValidator;
    }


    public void login(String idClient, String pinCode) {
        Client client = null;
        try {
            client = server.getClientById(idClient);
            pinValidator.validator(client, pinCode);
        } catch (AccountIsLockedException e) {
            System.err.println(e.getMessage());
        }

    }

    @Override
    public void creditOfAccount(String idClient, Integer summ) {

        if (server.getClientById(idClient).isLoginClient()) {
            try {
                server.creditAccount(idClient, summ);
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }

    }

    @Override
    public void depositOfAccount(String idClient, Integer summ) {
        if (server.getClientById(idClient).isLoginClient()) {
            try {
                server.depositAccount(idClient, summ);
            } catch (TransitException e) {
                System.err.println(e.getMessage());
            }
        }
    }

    @Override
    public void presetSummAccount(String idClient) {
        if (server.getClientById(idClient).isLoginClient()) {
            System.out.println("На счету " + server.getSummAccount(idClient) + "руб.");
        }
    }
}
