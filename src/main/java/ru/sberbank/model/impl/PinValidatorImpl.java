package ru.sberbank.model.impl;

import org.springframework.stereotype.Service;
import ru.sberbank.model.entity.Client;
import ru.sberbank.model.service.PinValidator;
import ru.sberbank.view.exception.AccountIsLockedException;

import java.util.Date;

@Service
public class PinValidatorImpl implements PinValidator {

    @Override
    public boolean validator(Client client, String pin) throws AccountIsLockedException {
        boolean pinIsValid = client.getPinCode().equals(pin);
        if (pinIsValid) {
            client.setAttempts(3);
            client.setLoginClient(true);
            System.out.println("Авторизация прошла успешно");
            return true;
        } else {
            if (client.getAttempts() > 0) {
                client.setAttempts(client.getAttempts() - 1);
                return false;
            } else {
                client.setDateFinishedBlock(new Date(new Date().getTime() + 5000L));
                throw new AccountIsLockedException("Клиент заблокирован на 5 сек");
            }
        }
    }
}
