package ru.sberbank.model.impl;

import ru.sberbank.model.entity.Client;
import ru.sberbank.model.entity.ClientDAO;
import ru.sberbank.view.exception.AccountIsLockedException;
import ru.sberbank.view.exception.TransitException;

import java.util.Date;

public class TerminalServer {

    private ClientDAO clientDAO;

    public TerminalServer() {
        this.clientDAO = ClientDAO.initializeClientDAO();
    }

    public Integer getSummAccount(String idClient) {
        return clientDAO.getClientMap().get(idClient).getSummAccount();
    }

    public void creditAccount(String idClient, Integer summTransfer) {
        if (summTransfer % 100 != 0) {
            throw new TransitException("Сумма не кратна 100");
        } else if (summTransfer > clientDAO.getClientMap().get(idClient).getSummAccount()) {
            throw new TransitException("Сумма снятия превышает остаток по счету");
        } else {
            int summAccount = clientDAO.getClientMap().get(idClient).getSummAccount();
            clientDAO.getClientMap().get(idClient).setSummAccount(summAccount - summTransfer);
            System.out.println("Операция прошла успешно. Остаток по счету: " + clientDAO.getClientMap().get(idClient).getSummAccount() + "руб");
        }
    }

    public void depositAccount(String idClient, Integer summTransfer) {
        if (summTransfer % 100 != 0) {
            throw new TransitException("Сумма не кратна 100");
        } else {
            int summAccount = clientDAO.getClientMap().get(idClient).getSummAccount();
            clientDAO.getClientMap().get(idClient).setSummAccount(summAccount + summTransfer);
            System.out.println("Операция прошла успешно. Остаток по счету: " + clientDAO.getClientMap().get(idClient).getSummAccount() + "руб");
        }
    }

    public Client getClientById(String idClient) throws AccountIsLockedException {
        Client client = clientDAO.getClientMap().get(idClient);
        if (client.getDateFinishedBlock() != null && new Date().getTime() < client.getDateFinishedBlock().getTime()) {
            throw new AccountIsLockedException("Клиент заблокирован");
        }
        return client;
    }
}
