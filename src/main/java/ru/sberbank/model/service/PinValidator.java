package ru.sberbank.model.service;

import ru.sberbank.model.entity.Client;
import ru.sberbank.view.exception.AccountIsLockedException;

public interface PinValidator {
    boolean validator(Client client, String pin) throws AccountIsLockedException;
}
