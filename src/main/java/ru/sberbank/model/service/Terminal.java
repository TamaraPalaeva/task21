package ru.sberbank.model.service;

public interface Terminal {
    void login(String idClient, String pinCode);

    void creditOfAccount(String idClient, Integer summ);

    void depositOfAccount(String idClient, Integer summ);

    void presetSummAccount(String idClient);
}
