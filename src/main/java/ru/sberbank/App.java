package ru.sberbank;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.sberbank.controller.TerminalControllerImpl;
import ru.sberbank.model.impl.TerminalService;

public class App {
    public static void main(String[] args) throws InterruptedException {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        TerminalControllerImpl controller = new TerminalControllerImpl(context.getBean("service", TerminalService.class));

        controller.login("1", "1234");
        controller.login("1", "1234");
        controller.login("1", "1234");
        controller.login("1", "1234");


        controller.login("1", "1234");

        Thread.sleep(6000);

        controller.login("1", "9876");

        controller.presetSummAccount("1");

        //service.creditOfAccount("1", 2541);
        controller.creditOfAccount("1", 25000);
        controller.creditOfAccount("1", 2500);

        // service.depositOfAccount("1", 2451);
        controller.depositOfAccount("1", 7100);

        context.close();
    }
}
