package ru.sberbank.view.exception;

public class TransitException extends RuntimeException {

    public TransitException(String message) {
        super(message);
    }

}

