package ru.sberbank.testService;


import org.junit.Assert;
import org.junit.Test;
import ru.sberbank.model.entity.Client;
import ru.sberbank.model.impl.TerminalServer;

public class TestServer {

    @Test
    public void testServer(){

        Client client = new Client("Ivanod", "1", "9876", 6542);

        Client testClient = new TerminalServer().getClientById("1");

        Assert.assertEquals(client, testClient);

    }



}
